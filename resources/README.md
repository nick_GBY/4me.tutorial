# 4ME Resources

Curated list of resources which influenced 4ME design.

## Websites / Blogs
* [/r/javascript](https://reddit.com/r/javascript)
* [/r/reactjs](https://reddit.com/r/reactjs)
* [/r/devops](https://reddit.com/r/devops)
* [/r/docker](https://reddit.com/r/docker)
* [/r/sysadmin](https://reddit.com/r/sysadmin)

## DevOps
* [**BOOK** The Phoenix Project](https://www.amazon.com/Phoenix-Project-DevOps-Helping-Business/dp/0988262592)
* [**BOOK** The DevOps Handbook](https://www.amazon.com/DevOps-Handbook-World-Class-Reliability-Organizations/dp/1942788002)
* [**BOOK** Infrastructure as Code](https://www.amazon.com/Infrastructure-Code-Managing-Servers-Cloud/dp/1491924357)

## Infrastructure
* [**VIDEO** GOTO 2016: The Future of Software Engineering](https://www.youtube.com/watch?v=6K4ljFZWgW8)
* [**BLOG** That app you love](http://developerblog.redhat.com/2016/09/27/that-app-you-love-part-1-making-a-connection/)
* [**REDDIT** Avoid putting secrets or credentials in Git](https://www.reddit.com/r/devops/comments/502n8u/avoid_put_secrets_or_credentials_in_git/)
* [**BLOG** Docker Security in production](https://www.delve-labs.com/articles/docker-security-production-2/)

## Software engineering
* [**VIDEO** Cheng Lou - On the spectrum of abstraction](https://www.youtube.com/watch?v=mVVNJKv9esE)
* [**VIDEO** Sebastian Markbage - Minimal API Surface Area](https://www.youtube.com/watch?v=4anAwXYqLG8)

## Javascript
* [**VIDEO** The fundamentals of Flow in 10-ish minutes](https://www.youtube.com/watch?v=xWMuAUbXcdQ)
* [**BLOG** Javascript Competency Matrix](https://medium.com/@TuckerConnelly/javascript-competency-matrix-6831817885d9#.gu8qz87mj)
* [**BLOG** JavaScript and Functional Programming](https://bethallchurch.github.io/JavaScript-and-Functional-Programming/)
* [**E-BOOK** You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)

## Backend
* [**BLOG** Node.js Monitoring Done Right](https://hackernoon.com/node-js-monitoring-done-right-70418ecbbff9#.gm80ggud7)
* [**BLOG** 19 things I learnt reading the NodeJS docs](https://hackernoon.com/19-things-i-learnt-reading-the-nodejs-docs-8a2dcc7f307f#.eznbuml8m)
* [**BLOG** NodeJS Microservices : From Zero to Hero](https://articles.microservices.com/nodejs-microservices-from-zero-to-hero-pt1-279548cb4080#.mt0ze4k3l)

## Frontend
* [**BLOG** So you want to learn React.js?](https://edgecoders.com/so-you-want-to-learn-react-js-a78801d3cd4d#.u2k3x9g45)
* [**VIDEO** React Router v4 with Michael Jackson and Ryan Florence](https://www.youtube.com/watch?v=Vur2dAFZ4GE)
* [**VIDEO** Ryan Florence - <Rethinker stop={false} />](https://www.youtube.com/watch?v=kp-NOggyz54)
* [**VIDEO** Lee Byron - Immutable User Interfaces](https://www.youtube.com/watch?v=pLvrZPSzHxo)
* [**VIDEO** Jem Young - Embracing The Future](https://www.youtube.com/watch?v=CRjGt0KfjzE)
* [**VIDEO** Kate Hudson - Writing better multi-process hybrid apps with React and Redux](https://www.youtube.com/watch?v=CciUEEkqHXU)
* [**BLOG** Tips For a Better Redux Architecture: Lessons for Enterprise Scale](https://hashnode.com/post/tips-for-a-better-redux-architecture-lessons-for-enterprise-scale-civrlqhuy0keqc6539boivk2f)
* [**BLOG** Redux Patterns and Anti-Patterns](https://tech.affirm.com/redux-patterns-and-anti-patterns-7d80ef3d53bc#.pvcbgxiai)
* [**VIDEO** Dan Abramov : The Redux Journey](https://www.youtube.com/watch?v=uvAXVMwHJXU)
