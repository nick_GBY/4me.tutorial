# 4ME Tutorials

## Introduction
Installing, running and modifying a 4ME install requires a bit of knowledge. 4ME is a complicated system, maintaining it is also complicated even though we're trying hard to keep it manageable.

This repository contains multiple tutorials to bring you up to speed with the system as a whole.

## Prerequisites
This tutorial assumes the following :
* Your dev environment is either OSX or linux. 4ME uses Docker and NodeJS which tend to have issues on Windows
* Basic shell knowledge (typing commands, navigating between directories)
* Basic git knowledge (clone a repo, navigate between branches)

## List of tutorials
* [First steps in 4ME : apprehending Docker][first_steps]

This tutorial will cover the first steps involved to get a running 4ME installation. At the end of this tutorial, you'll have a working barebone 4ME install composed of a working UI with a working control room map service.

## Resources

This repository also contains a list of resources deemed interesting to better understand choices and trade-offs made during 4ME development.

See the list of resources in the [resources directory][resources].

## FAQ
* [Questions raised by CRNA-SO (2017-01-19)][faq] (french)

[first_steps]: ./first_steps
[resources]: ./resources
[faq]: ./FAQ.md
