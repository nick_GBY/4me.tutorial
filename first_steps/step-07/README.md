# Step 07 - docker-compose

> Objectives:
> * What is docker-compose purpose ?
> * Create a skeleton docker-compose.yml file.

## Prerequisites
This step assumes you have completed [step 05][step-05]  and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

You should also have a running container :
* `4me_frontend`, running the image `4me.frontend:v1.0.0`, with port 80 published and a proper config file injected.
* `4me_core_mapping`, running the image `4me.core.mapping:v0.2.0`, with port 3200 published and a data volume injected.

You should also have a directory called `/data` on your workstation with this file structure :
```
/data
├── 4me.core.mapping
│   └── db
│       ├── 000003.log
│       ├── CURRENT
│       ├── LOCK
│       ├── LOG
│       └── MANIFEST-000002
└── 4me.frontend
    └── config.api.js
```

The content of `/data/4me.core.mapping/db` is not relevant. You should just have some files in there.

## One file to rule them all

We've seen in previous steps that we must type very long `docker run` commands to get a container running the way you want it to run. This process is awkward and error-prone. One could make a typo and inject a wrong volume, or forget to publish a specific port. This could make our whole stack useless.

What if we could *describe* our whole container stack in a single configuration file ? That's exactly the purpose of `docker-compose`, a handy tool written by the docker team.

Docker-compose main input is a `docker-compose.yml` file. It's a [YAML][YAML] file, and we'll use it to describe and run our application stack. The name of the tool suggests it : `docker-compose` goal is to *compose* containers to make a single application stack.

To make things easier on your end, I brought a [docker-compose.yml][docker-compose] file with me. I'll copy the content here to discuss it :

```YAML
4me_frontend:
  image: registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
  container_name: 4me_frontend
  volumes:
    - /data/4me.frontend/config.api.js:/usr/share/nginx/html/js/config.api.js:ro
  ports:
    - "80:80"

4me_core_mapping:
  image: registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0
  container_name: 4me_core_mapping
  environment:
    - PORT=3200
  volumes:
    - /data/4me.core.mapping/db:/usr/src/app/db.up
  ports:
    - "3200:3200"
```

If you remember the previous steps correctly, reading this file should be fairly straightforward. Remember, in our case, the purpose of this file is to avoid typing long `docker run commands`.

## Using `docker-compose` with your 4ME stack

Let's use this file to run our application stack.

First, you must remove the previous containers :
```
# docker rm -f 4me_frontend 4me_core_mapping
4me_frontend
4me_core_mapping
```

Now, create or download the [provided docker compose file][docker-compose] to `/data/docker-compose.yml`.

Then open a terminal in `/data` directory and type :
```
# docker-compose pull
Pulling 4me_frontend (registry.gitlab.com/devteamreims/4me.frontend:v1.0.0)...
v1.0.0: Pulling from devteamreims/4me.frontend
Digest: sha256:4140413482abc2793773fddd709baf5c64a12cc64cf93e5952dac36374b94642
Status: Image is up to date for registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
Pulling 4me_core_mapping (registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0)...
v0.2.0: Pulling from devteamreims/4me.core.mapping
Digest: sha256:d78c0361a4abb0fd4346b3c431fac7212c6b07cb136c1cf09c70044a863d8a25
Status: Image is up to date for registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0
```
As you already might have guessed from the output, this instructs docker compose to *pull* the necessary images from the internet. This step is not necessary since you already have the required images on your disk.

Now we'll instruct docker-compose to *create* the containers but not run them yet :
```
# docker-compose create
Creating 4me_core_mapping
Creating 4me_frontend

# docker ps -a
CONTAINER ID        IMAGE                                                      COMMAND                  CREATED             STATUS              PORTS               NAMES
72b6d2743f12        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0       "nginx -g 'daemon off"   27 seconds ago      Created                                 4me_frontend
817018e40e6b        registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0   "npm start"              28 seconds ago      Created                                 4me_core_mapping
```

Great, we have the containers created and ready to go. Let's run them :
```
# docker-compose start
Starting 4me_core_mapping ... done
Starting 4me_frontend ... done

# docker ps
CONTAINER ID        IMAGE                                                      COMMAND                  CREATED              STATUS              PORTS                              NAMES
72b6d2743f12        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0       "nginx -g 'daemon off"   About a minute ago   Up 9 seconds        0.0.0.0:80->80/tcp, 443/tcp        4me_frontend
817018e40e6b        registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0   "npm start"              About a minute ago   Up 9 seconds        3100/tcp, 0.0.0.0:3200->3200/tcp   4me_core_mapping
```

If you open your browser, point to `http://localhost` and refresh the page, you should see the app up and running, just like at the end of [step 06][step-06].

Let's stop these containers :
```
# docker-compose stop
Stopping 4me_frontend ... done
Stopping 4me_core_mapping ... done

# docker ps -a
CONTAINER ID        IMAGE                                                      COMMAND                  CREATED             STATUS                      PORTS               NAMES
72b6d2743f12        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0       "nginx -g 'daemon off"   4 minutes ago       Exited (0) 11 seconds ago                       4me_frontend
817018e40e6b        registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0   "npm start"              4 minutes ago       Exited (0) 11 seconds ago                       4me_core_mapping
```

And remove them:
```
# docker-compose rm
Going to remove 4me_frontend, 4me_core_mapping
Are you sure? [yN] y
Removing 4me_frontend ... done
Removing 4me_core_mapping ... done

# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

## Shortcuts: `docker-compose up` and `docker-compose down`
Docker-compose provides two convenients shortcuts for managing a composed container stack.

The first one is `docker-compose up` which will :
* pull the required images
* create the containers
* start them

> Note that by default, docker-compose will not run the containers in `detached` mode. Therefore you must provide a `-d` flag to run them in the background.
> Feel free to run the containers in `attached` mode. You'll see a bunch of stuff happening in your terminal. This is the log output of the running containers described in the docker-compose file.

```
# docker-compose up -d
Creating 4me_core_mapping
Creating 4me_frontend

# docker ps
CONTAINER ID        IMAGE                                                      COMMAND                  CREATED             STATUS              PORTS                              NAMES
59a9d3765bff        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0       "nginx -g 'daemon off"   5 seconds ago       Up 3 seconds        0.0.0.0:80->80/tcp, 443/tcp        4me_frontend
69093f3e59de        registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0   "npm start"              5 seconds ago       Up 3 seconds        3100/tcp, 0.0.0.0:3200->3200/tcp   4me_core_mapping
```

Likewise, we can use the `docker-compose down` command to stop and remove the containers described in docker-compose file.

```
# docker-compose down
Stopping 4me_frontend ... done
Stopping 4me_core_mapping ... done
Removing 4me_frontend ... done
Removing 4me_core_mapping ... done

# docker ps -a
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

## Moving on

This is the end of this tutorial. Hopefully now, you have a better sense of we orchestrate multiple pieces of software in 4ME to create a full applicative stack.

Since compose files are just text files, we keep them in a git repo. That way, we can track changes done to the stack and revert to a previous good configuration if things go wrong in production. With the help of this compose file, we can now trace the state of the applicative stack through time. If we were asked "What were the versions deployed on this particular date ?", we'd be able to answer just by looking at our git history.

This tutorial just scratched the surface of what docker has to offer. Here's a curated list of ressources about docker to go deeper : https://github.com/veggiemonk/awesome-docker

Thank you for your time, file [an issue][issue-tracker] if you spot a mistake or feel like some section is unclear.



[docker-compose]: ./docker-compose.yml
[YAML]: http://yaml.org/
[step-06]: ../step-06
[4me_devops]: https://gitlab.com/devteamreims/4me.devops/
[issue-tracker]: https://gitlab.com/devteamreims/4me.tutorial/issues
