# Step 01 - First steps with Docker

> Objectives:
> * How to pull 4ME images on your workstation
> * How to check those images are installed
> * How to remove an image

4ME makes a heavy use of [Docker][Docker]. Docker is a microvirtualization framework which allows developpers to *package* their application and their dependencies in a single immutable **image**.

An image is then used by Docker to create a **container**, which is a running instance of a single image. See the difference here : http://stackoverflow.com/a/26960888/194685

The idea here is not to make an extensive tutorial about Docker. There are plenty of those around on the internet.

## Pull your images
In order to prepare our application stack, we must first download the docker images to your local machine.  
Usually, when installing a piece of software, you'd receive a tarball or use a package manager. Docker is able to *pull* images from a remote repository directely called an **image registry**.

The command we'll use is called `docker pull`. To get a minimal 4ME install, we need 2 images :
* `4me.frontend`
* `4me.core.mapping`

In a terminal type this first command :
```
# docker pull registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
```

> If you have a poor internet access, now would be a good time to grab a coffee :).

This will instruct docker to pull the docker image called `4me.frontend`, tagged `v1.0.0`, from the namespace `devteamreims` of the `registry.gitlab.com` docker registry.

Your terminal should now look like this :
```
v1.0.0: Pulling from devteamreims/4me.frontend
386a066cd84a: Pull complete
386dc9762af9: Pull complete
d685e39ac8a4: Pull complete
39d4e70a65ec: Pull complete
Digest: sha256:4140413482abc2793773fddd709baf5c64a12cc64cf93e5952dac36374b94642
Status: Downloaded newer image for registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
```

You should see a bunch of stuff happening in your terminal. It's important to note that docker images are *layered*. This means we can build an image based on other images. What we see here is each layer being downloaded then extracted by the docker engine.

Now we can pull the other required image :
```
# docker pull registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0
```
> Again, coffee time !

## List and manipulate installed images
Docker allows you to list all installed docker images via the command `docker images`.

At this point, this should look like this :
```
# docker images
REPOSITORY                                          TAG                 IMAGE ID            CREATED             SIZE
registry.gitlab.com/devteamreims/4me.core.mapping   v0.2.0              2c6c1980e9d9        About an hour ago   800.1 MB
registry.gitlab.com/devteamreims/4me.frontend       v1.0.0              744d2f5b301a        2 hours ago         199.6 MB
```

Docker also allows you to remove an image from your workstation via the command `docker rmi`
You can remove an image using it's full name :
```
# docker rmi registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
```
Or by using its *tag* :
```
# docker rmi 744d2f5b301a
```

> For those poor souls with a slow internet access, don't run these commands unless you want to grab another coffee

## Moving on
If you've actually removed the images, it's time to redownload them since we'll need them in [step 02][step-02].

During the next steps, we'll focus on taming this wild beast called Docker. For this, we'll only use the frontend application as a guinea pig.


[Docker]: https://docker.com/
[step-02]: ../step-02
