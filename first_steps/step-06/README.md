# Step 06 - Configuring `4me.core.mapping`

> Objectives:
> * Get 4me.core.mapping running with proper configuration

## Prerequisites
This step assumes you have completed [step 05][step-05]  and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

You should also have a running container :
* `4me_frontend`, running the image `4me.frontend:v1.0.0`, with port 80 published and a proper config file injected.

## Let's bring some data into `4me.frontend`

We left [step 05][step-05] with a working frontend app but without any backend service to provide data.

We'll now introduce the `4me.core.mapping` image that's been lying around on your computer since [step 01][step-01] .

This piece of software has multiple roles :
* It's reponsible for identifying 4me clients. When a clients sends a specific request, the backend service should be able to respond "hi dear 4me client ! you're client 1, you're a supervisor" or "you're client 23, you're a CWP and your sectors are UR and XR"
* It's also responsible for maintaining the control room state, i.e that sector UR and XR are bound to client 23 for instance
* It's also reponsible for notifying clients that the control room state has been updated ("client 23, you now have sector UR, XR, KR and HYR")

## Stateful vs stateless container
A little bit of theory before moving on. Containers are often described as perfect for *stateless* applications. Containers come and go, and ideally, to allow easy replacement, they shouldn't contain any piece of state inside them.

Our `4me.core.mapping` app contains an important piece of state : the control room sector distribution. We could hold onto this piece of state inside the container but whenever we replace our container, this piece of state would disappear. This means that when we start a new container (like when we want to upgrade our app), the supervisor would have to reconfigure the whole control room. Not a great user experience right ?

Docker offers various ways to make *stateful* apps with *stateless* containers :
* We could store the state in a remote database
* We could store the state on disk, in a mounted volume with read-write permissions

With state out of the container, our *stateful* app now relies on a *stateless* container, which can be removed or replaced.

## Back to `4me.core.mapping`
For this particular service, we opted to put the state on the host disk, and to use a mounted volume to do so.

Let's prepare the host (your workstation). Again, I'll keep the same convention of the `/data` directory.
```
# mkdir -p /data/4me.core.mapping/db
```

There. That's it. We just created an empty directory which will hold our precious state. The image will store it's database in `/usr/src/app/db.up`, which is a path **inside** the container. We'll mount our host directory to this path later on.

Now our image needs a bit more to properly work. It also needs a port to accept incoming requests from 4me clients. By default, a container running this image will expose port 3100 ([it's defined in the image Dockerfile](https://gitlab.com/devteamreims/4me.core.mapping/blob/master/Dockerfile)). But remember [step 05][step-05] ! We told our frontend app that `4me.core.mapping` should be reachable on port 3200 ! Multiple solutions here :
* We could tell our app inside the container to expose port 3200 and have the host publish port 3200 on port 3200
* We could tell our app nothing, let it expose whatever port it pleases and have the host publish the proper port on port 3200

We went with solution 1 for a simple reason : intercontainer communication. When a container exposes a port, this specific port is reachable for other containers in the same Docker network. When the host publishes port A (on container X) on port B (on the host), port A (container X) becomes reachable through port B (on the host). But other containers still need to reach container X through port A. For consistency stake we decided to have a 1:1 mapping between ports in the docker network and ports on the outside.

## The run command
Let's bring all of this together. We'll run a container called `4me_core_mapping`, inject our data folder in the right place inside, tell the app to run on port 3200, and publish this exposed port to the port 3200 of the host.
```
# docker run -d --name 4me_core_mapping -v /data/4me.core.mapping/db:/usr/src/app/db.up:rw -e "PORT=3200" -p 3200:3200 registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0
2ae22d9fbb20bc6a496208ba0d62d718ab625571e1b2551524420137ede9a668
```
> Tired of writing those long commands in your terminal ? [Step 07][step-07] will come to the rescue !

## Bringing it all together

Remember that browser window pointing to `localhost` ? Yeah, refresh it.
![step07][step_07_image]

**B-E-A-U-T-I-F-U-L**

## Playing around a bit
Now, I know you want to play around with your new toy. But for now, you can't do much in the UI. That's expected behaviour. Remember in [step 05][step-05] when we set `overrideCwpId` to the value `37` in `config.api.js` ? The app running in your browser is acting like it's the CWP #37 in LFEE control room. And since it's a CWP, you're not allowed to change the control room layout.

Let's tweak our config file to become a supervisor. Open the config file which should be in `/data/4me.frontend/config.api.js` and replace `overrideCwpId: 37,` by `overrideCwpId: 1,`.
> Note the trailing comma, this is JavaScript !

Refresh... And nothing happened, you are still stuck with CWP37 ! Wait, didn't you say that by mounting the file `/data/data/4me.frontend/config.api.js` in the `4me.frontend` container, this container should see the exact same `config.api.js` file as seen from the host with the lastest changes ?
You are absolutely right ! The `4me_frontend` container does indeed have access to the updated version of `config.api.js` file with CwpId set to 1. The problem here lies in nginx that, if not instructed to do otherwise, will cache static files.

You've guest it, the quick fix here is to stop the `4me_fronted` container and restart which will in turn restart the nginx and update its cache.

```
# docker restart 4me_frontend
```

And Voila !

## Moving on
You now have a minimal 4ME skeleton to play around. Now would be a good time to play around the Docker command, see what happens when you do this or that, break things, fix them, learn stuff in the process.

You'll notice you'll have to type these long `docker run` commands quite a bit. Fortunately Docker as a tool to make it much easier. It's called `docker-compose` and we'll talk about it in [step 07][step-07].

[step_07_image]: ../images/step07.png
[step-05]: ../step-05
[step-01]: ../step-01
