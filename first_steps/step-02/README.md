# Step 02 - Run your first container

> Objectives:
> * How to create a container from an image
> * How to stop and remove a container

## Prerequisites
This step assumes you have completed [step 01][step-01] and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

## From an image to a container
Reminder: a **container** is a specific instance of an **image**. See more here : http://stackoverflow.com/a/26960888/194685

Now that we have 4ME images, our first step will be to create and run a container serving the frontend application. The image for our container is `4me.frontend:v1.0.0`.

The command docker provides is `docker run`.

Let's try to run our first container. In a terminal type :
```
# docker run -d registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
```

The `-d` (or `--detach`) flag indicates that we want this container to run in `detached` mode. The container will run as a background process and won't

The output should look like this :
```
# docker run -d registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
275ce62b9f9bf222cc1b90f5cc060149cf0c17eb579db2e9f726bfe5b4397942
```
> This long string of characters docker spits out is the unique id of the running container.

There. You now have a running container. You just can't do anything with it yet. We'll cover this later.

To list all containers, docker provides the command `docker ps`, by analogy with the regular `ps` command.
```
# docker ps
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS              PORTS               NAMES
275ce62b9f9b        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   3 minutes ago       Up 3 minutes        80/tcp, 443/tcp     modest_aryabhata
```

Ok, that's a lot of information here :
* `CONTAINER ID` is the short version of the unique id docker generated for you CONTAINER
* `IMAGE` is the name of the image used to run this container
* `COMMAND` is the command docker executed **inside the container** to boot up the application
* `CREATED` is when this specific container was created
* `STATUS` indicates that the container has been up for 3 minutes
* `PORTS` list all ports opened inside the container
* `NAMES` is the human readable name attributed by the container. Since we didn't provide one when we started the container using `docker run`, docker was kind enough to generate one for us.

> Even though port 80 and 443 are listed here, these ports are still restricted to the docker network stack. No one can access your app from the outside yet. We'll cover this later in [step 03][step-03].

At this point, you can even start another one and give it a proper name
```
# docker run -d --name my_second_container registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
7f5976e3ddab65546d22c5dda1bdd3283964be083e8b022ef3a297363c226877
# docker ps
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS              PORTS               NAMES
7f5976e3ddab        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   15 seconds ago      Up 14 seconds       80/tcp, 443/tcp     my_second_container
275ce62b9f9b        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   11 minutes ago      Up 11 minutes       80/tcp, 443/tcp     modest_aryabhata
```

At this point, we have 2 containers based on the same image running in the background.

## Stopping a container, listing all containers on your workstation
To stop a container, docker provides a `docker stop` command.
It takes a container id or a container name as argument.
```
# docker stop my_second_container
my_second_container
```
When a container is stopped, it no longer appears in the output of the `docker ps` command :
```
# docker ps
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS              PORTS               NAMES
275ce62b9f9b        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   13 minutes ago      Up 13 minutes       80/tcp, 443/tcp     modest_aryabhata
```

`docker ps` will only show *running* containers. If you want to see *every* container on your workstation, you need to pass the `-a` or `--all` flag.
```
# docker ps -a
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS                          PORTS               NAMES
7f5976e3ddab        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   3 minutes ago       Exited (0) About a minute ago                       my_second_container
275ce62b9f9b        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   14 minutes ago      Up 14 minutes                   80/tcp, 443/tcp     modest_aryabhata
```

## Removing containers
At this point, we have a running container and a stopped container. To prepare for [step 03][step-03], let's remove those 2 containers.  
Docker provides a `docker rm` command to do so.  
First remove the stopped container :
```
# docker rm my_second_container
my_second_container
# docker ps -a
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS              PORTS               NAMES
275ce62b9f9b        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   17 minutes ago      Up 17 minutes       80/tcp, 443/tcp     modest_aryabhata
```

Now let's try to remove the remaining one :
> Note: you can also target a container by its container id.

```
# docker rm 275ce62b9f9b
Error response from daemon: You cannot remove a running container 275ce62b9f9bf222cc1b90f5cc060149cf0c17eb579db2e9f726bfe5b4397942. Stop the container before attempting removal or use -f
```

Well, I'm sure you can figure out what to do next to remove the container right ?

## Moving on
At this point, you should still have 2 images installed. You should also have no container running.

[step-03]: ../step-03
[step-01]: ../step-01
