# Step 04 - Injecting stuff in a container

> Objectives:
> * How to inject environment variables into a container
> * How to inject a file into a container

## Prerequisites
This step assumes you have completed step 03 and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

You should also have no container running.

## Configuration of immutable images
Docker images are immutable. They are artifacts. They're downloaded from a registry and that's it. The whole point is that you don't modify them locally so we can guarantee the software you run is the same software that we run, and that was ran through our testing pipeline.

At this point, you're probably asking how configuration is supposed to be done.

Docker offers two mecanisms that are used in 4ME : volumes and environment variables.

## Environment variables
When you run a container, docker allows you to pass any environment variable you want to the container environment when you start it with `docker run` with the flag `-e` or `--env`.

A sample command would look like this :
```
# docker run -d -e "test_var=foo" sample_image
```

This command will inject an environment variable called `test_var` with the value of `foo` inside the container.

You can also pass multiple environment variables :
```
# docker run -d -e "test_var=foo" -e "test_var_2=bar" sample_image
```

You can even pass a `envfile` via the `--envfile` flag.

4ME uses this particular technique for configuration of other containers in the stack.

Now regarding our guinea pig image `4me.frontend`, the error message in the browser console was about some missing *file* ([here for the lazy !](../images/step-03.png)).

We now know about inject environment variables, but what about actual *files* ?

## Volumes
Docker allows you to do so. It's called `volumes` in Docker-land.  
What it does is mount a specific file or directory *from the host inside a container*. Like the env variable stuff, you get to inject files or directories in the container using the `-v` or `--volume` flag of the `docker run` command.

The syntax is fairly straightforward :
```
# docker run -d -v /fullpath/to/file-on-host:/path/inside/container/for/file-on-host sample_image
```

You can mount whole directories too :
```
# docker run -d -v /fullpath/to/directory-on-host:/path/inside/container sample_image
```

By default, docker will give read-write privileges to the file to the container environment. This behaviour can be changed by adding `:ro` at the end of the `-v` parameter :
```
# docker run -d -v /fullpath/to/file-on-host:/path/inside/container/for/file-on-host:ro sample_image
```

## Moving on
We haven't done much here but talk. We'll put those principles in practise in [step 05][step-05] to properly configure `4me.frontend`.

Now would be a good time to dive a bit deeper into Docker since we've just scratched the surface here. There are numerous tutorials about that on the internet. Make sure you don't read outdated stuff : Docker is moving fast and API changes happen.

[step-03][../step-03]
