# Step 03 - Opening a container to the outside world

> Objectives:
> * A quick view of Docker network stack
> * How to make the container talk to the outside world

## Prerequisites
This step assumes you have completed [step 02][step-02] and have those 2 images installed on your workstation :
* registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
* registry.gitlab.com/devteamreims/4me.core.mapping:v0.2.0

You should also have no container running.

## The docker network stack
Docker containers live happily on their own network stack, disconnected from your regular network stack. This separe network stack really shines in complicated setups (limiting outside access to containers, clustering, isolation between container stacks, high availability). You can read [more here](https://docs.docker.com/engine/userguide/networking/).

For now, we won't dive deep into the docker network stack, we'll just use it.

Ok, here's the million dollar question : I have a container, running and accepting connections on a specific port in the docker network stack, how can I make it accept connections from the REAL WORLD ?!

The process to answer this question is called *publishing ports* in docker language. Basically, you tell docker to *publish* to the outside world a port from a container.

> Note: A container *exposes* a port. Docker *publishes* an exposed port.

## Publishing ports in our 4ME stack
To publish a port, you use the flag `-p` or `--publish` on the `docker run` command.

Now, a bit of information about the `4me.frontend` image. This image's sole purpose is to deliver our frontend application to 4ME clients. Our frontend app is a JavaScript app, 4ME clients are web browsers. You guessed it, `4me.frontend` is just a web server (nginx in this case) delivering static files composing our frontend app.

4me.frontend exposes 2 ports : `80` and `443`. Since we don't care about SSL, let's focus on the port 80.

Let's run our container again, but this time we'll publish the exposed port 80.
```
# docker run -d -p 80 --name 4me_frontend registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
4me_frontend
```

Now we should be able to connect to our container !

```
# curl localhost
curl: (7) Failed to connect to localhost port 80: Connection refused
```

WHAT ?  
We told docker to publish the exposed port 80. We didn't tell docker to what external port it should be published though.
```
# docker ps
CONTAINER ID        IMAGE                                                  COMMAND                  CREATED             STATUS              PORTS                            NAMES
3d3dd9f9ddb2        registry.gitlab.com/devteamreims/4me.frontend:v1.0.0   "nginx -g 'daemon off"   3 minutes ago       Up 3 minutes        443/tcp, 0.0.0.0:32768->80/tcp   4me_frontend
```
As you can see, the `PORTS` column has changed. What docker is telling us is that the port 80 of the container is now reachable through the port 32768 of our host.

```
# curl localhost:32768
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>4ME</title>
</head>
<body>
<script type="text/javascript" src="/js/config.api.js"></script>

<div id="app"></div>

<script type="text/javascript" src="/js/app.js"></script><script type="text/javascript" src="/js/vendor.js"></script></body>
</html>
```

And there goes our app.

Now, having a random port is not very convenient. Fortunately, docker allows us to specify which outside port we want for a specific container port. The syntax is `-p hostPort:containerPort`.

Let's trash and rerun our container to accept connections on the port 80 :
```
# docker rm -f 4me_frontend
4me_frontend
# docker run -d -p 80:80 --name 4me_frontend registry.gitlab.com/devteamreims/4me.frontend:v1.0.0
27d8a6ceab4cd841e0ab17dcea4cf706149f3ff316d90d0cdd3c673734d3965b
```

## Moving on
At this point, we have a running container, listening for outside connections on port 80. Let's plug a web browser on `http://localhost:80` to see what the container gives us.

You should be looking at a grey empty screen. And if you bring up the console in your browser, you should have something like this :
![step03](../images/step03.png)

Now, this is normal. Our app is not configured yet and it's complaining about a missing `config.api.js` file required to run. We'll get to this in [step 04][step-04].

Let's clean up before moving on :
```
# docker rm -f 4me_frontend
4me_frontend
```

[step-04]: ../step-04
[step-02]: ../step-02
