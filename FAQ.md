# Questions diverses posées à l'intérieur de la DSNA

### Pouvez-vous préciser les logiciels/langages utilisés et ce qui a motivé leur utilisation ? Pourriez-vous nous présenter l'architecture de l'application, le rôle de docker ?

Dès la génèse du projet, plusieurs besoins essentiels ont été identifiés. Nous voulions une IHM riche, interactive, multiservices, multiutilisateurs et facile d'utilisation : riche et multiservices car regroupant divers modèles de données, provenant de sources différentes, interactive car nous voulions être capables de pousser de l'information depuis l'extérieur vers les utilisateurs et multiutilisateurs car devant s'adapter à une utilisation par un contrôleur sur position, comme un chef de salle ou un FMPiste. Un autre aspect crucial relevé consistait en faciliter au maximum les changements. Être agile, en essence peut se résumer en 4 étapes ([GOTO 2015 / Agile is Dead / Dave Thomas](https://www.youtube.com/watch?v=a-BOSpxYJ9M)):
* Evaluer l'existant
* Faire un petit pas vers l'objectif
* Ajuster l'évaluation de l'existant en intégrant les leçons du petit pas vers l'objectif
* Recommencer

Concrètement, cela revient à dire qu'on ne peut pas construire un système parfait du premier coup. On peut dépenser une énergie considérable à travailler les spécifications et toujours rater l'objectif : les besoins utilisateurs changent, on peut mal les comprendre, les utilisateurs peuvent mal exprimer leur besoin et le travail fourni peut se retrouver complètement inutile. Autoriser et faciliter le changement permet d'éviter cet écueil, en amenant les modifications très tôt vers les utilisateurs, puis en recueillant leurs retours et en corrigeant à la prochaine itération. J'insiste car cet aspect, pourtant pas tellement technique, a été crucial dans les choix d'architecture et de technologies pour ce projet.

En considérant l'ensemble de ces entrées, il apparaît une solution adaptée : un applicatif IHM basé sur des technologies web, alimenté par des services de données indépendants. Le fait d'utiliser un stack web permet d'avoir une IHM riche et intuitive. Ce choix permet également de se reposer sur un nombre important de librairies préexistantes dans le monde de l'OpenSource Software. Un stack web permet aussi de gérer les aspects interactifs grâce aux [WebSockets](https://fr.wikipedia.org/wiki/WebSocket). Le langage retenu a naturellement été JavaScript, qui est [le langage le plus populaire sur Github](http://githut.info/) et celui [qui présente l'écosystème le plus large](https://developers.slashdot.org/story/17/01/14/0222245/nodejss-npm-is-now-the-largest-package-registry-in-the-world). Un premier prototype a été écrit en utilisant [AngularJS](https://angularjs.org/), framework complet open source, traditionnel (organisation modèle-vue-controleur MVC classique) supporté notamment par Google. AngularJS, à l'époque, était en fin de vie. Ce choix a été réévalué par la suite pour se concentrer sur [ReactJS](https://facebook.github.io/react/), beaucoup plus restreint dans son scope, et très novateur. ReactJS, développé par Facebook, se concentre sur le V de MVC. ReactJS a amené avec lui de nouvelles pratiques, de nouvelles manières de développer. On pourrait en débattre des heures, mais je ne vais pas m'étendre pour ne pas rallonger un texte déjà long ([ici, une vidéo d'un des membres de l'équipe ReactJS chez Facebook pour compléter le propos](https://vimeo.com/166790294)).

A ce stade, nous avons une IHM, qui fonctionne dans un navigateur web, capable de consommer des données sur des services HTTP ou via WebSockets. Reste à se poser la question du design de ces services de données. Considérant les domaines très éloignés des services visés (XMAN, ETFMS PROFILE, services ATFCM comme STAM, Météo dans le futur), et considérant le besoin d'avoir des cycles de vie disjoints (services XMAN et ETFMS PROFILE, matures, qui devront cohabiter avec des services plus expérimentaux), le choix d'architecturer le backend en [microservices](http://microservices.io/patterns/microservices.html) semblait naturel.

A ce stade, voilà ce que nous savons :
* une codebase en JavaScript, s'appuyant sur ReactJS pour le frontend
* plusieurs microservices qui, en essence, devront prendre des données en entrée sur une source locale (base de données locale, webservice distant, microservice voisin) et exposer ou pousser une version transformée de ces données vers N applicatifs web (donc via une API HTTP ou WebSockets) avec assez peu de calculs lourds

Le choix s'est porté vers [NodeJS](https://nodejs.org/en/), environnement JavaScript côté serveur, profitant du même écosystème de librairies que l'applicatif frontend, très efficace sur les opérations d'entrée/sortie, s'intégrant naturellement avec les modes de transport spécifiques au monde du web (HTTP et WebSockets). NodeJS présente également deux avantages importants : permettre la réutilisation de code entre le frontend et le backend et limiter les changements de contexte mental pour l'équipe de développement.

A noter également, le choix d'une architecture en microservices permet de découpler totalement les services entre eux. Si à terme, on se rend compte que NodeJS n'est pas adapté à un service particulier, alors rien n'empêchera d'opter pour une technologie plus pertinente.

Pour résumer :
* un applicatif IHM Web
* une somme de microservices
  * avec des cycles de vie séparés
  * dont le nombre sera croissant au fil du temps
  * utilisant des technologies potentiellement hétérogènes
* une volonté de mettre en production les changements le plus tôt et le plus régulièrement possible

Bref, l'enfer pour un service technique ! :)

Docker intervient à ce niveau là. Docker est une forme de microvirtualisation. Docker utilise [LXC sous linux](https://fr.wikipedia.org/wiki/LXC), technologie similaire aux [jails de FreeBSD](https://fr.wikipedia.org/wiki/BSD_Jail) ou aux [zones Solaris](https://en.wikipedia.org/wiki/Solaris_Containers). Docker permet d'empaqueter un système de fichier, et de faire fonctionner un process dans un système de fichier controlé, sur un réseau virtualisé, en utilisant le kernel de la machine hôte. Concrètement, ça veut dire que le livrable produit contient l'applicatif et toutes ses dépendances. Grâce à Docker, je suis capable, en tant que développeur, d'empaqueter une large partie des dépendances applicatives. Côté opérations (ou service technique), le processus de mise en production d'une application se retrouve standardisé (déployer une appli NodeJS, ou PHP, ou Go est similaire).

Docker présente également de nombreux avantages :
* conserver une variance minimale entre les environnements de test et de production
* une API qui permet d'automatiser un certain nombre de choses
* un écosystème riche d'applications support

Bref, comme le reste des choix technologiques effectués, Docker solutionne un certain nombre de problêmes mais apporte également son lot de challenges. Il n'y a pas de solution parfaite. Docker par exemple remet en question un certain nombre de pratiques traditionnelles dans un service technique. Nous avons jugés que les avantages induits compensaient largement le ticket d'entrée.

Pour référence, voici l'architecture logicielle (Janvier 2017) où chaque boite représente un applicatif conteneurisé :
![archi](./images/archi.png)

### Comment se déroule le cycle de développement ?
Lorsqu'un besoin est exprimé, un ticket est créé dans notre issue tracker actuellement hébergé sur [gitlab.com](https://gitlab.com/devteamreims/4ME/issues). Les changements sont implémentés dans le code, via des merge requests. Gitlab permet d'assurer la traçabilité entre l'expression de besoin et les changements (voir ici : https://gitlab.com/devteamreims/4ME/issues/163). Le processus de merge request se prète parfaitement aux "code reviews", qui, lorsque l'équipe de développement s'agrandira, permettront de faire vérifier les changements par une autre paire d'yeux. L'objectif est d'intégrer les yeux d'experts (SSI par exemple) à ce stade du processus.

> Ces modifications peuvent parfois être "cachées" derrière un [feature flag](http://stackoverflow.com/questions/7707383/what-is-a-feature-flag) afin de découpler le déploiement d'une feature de sa mise a disponibilité des utilisateurs. Concrètement, 4ME contient du code "work in progress", désactivé pour les utilisateurs, activable par un simple changement de configuration. L'intérêt est qu'on peut déployer une feature et tout le code associé AVANT la mise en service de la fonctionnalité. Par exemple, on peut déployer du code quand on veut et n'activer les features que lorsque les utilisateurs auront été formés par exemple.

Quand un développeur pousse du code vers un dépôt, un certain nombre de tests automatisés sont lancés. Ce processus, appelé intégration continue (ou CI), permet dans l'absolu d'éviter un certain nombre de régressions. Une fois les tests automatiques validés, une image Docker est générée et est entreposée dans un dépôt. Cette image représente un livrable 4ME.

Cette image est ensuite déployée dans un environnement de test autohébergé (serveur personnel) et permet aux autres membres de l'équipe de valider les changements et de détecter d'inévitables bugs. Ce déploiement est actuellement partiellement automatisé (lancement d'un script [Ansible](https://www.ansible.com/)). L'objectif est de permettre au développeur d'obtenir un retour le plus rapidement possible sur ses changements.

Une fois les changements validés (et documentés : Note de service, NIT, etc.), ceux-ci sont déployés en production via un processus similaire.

### Avez-vous prévu une évolution du code pour tenir compte du déploiement sur les autres sites qui va nécessiter un paramétrage local ?

Absolument. Le parti pris a été d'utiliser des livrables (images Docker) communs entre tous les centres et d'injecter les particularités de chacun en configuration. Concrètement, une librairie d'environnement ([4me.env](https://gitlab.com/devteamreims/4me.env)) devra contenir toute la configuration d'environnement spécifique à chaque CRNA. Cette librarie est ensuite incluse dans son intégralité dans chaque image Docker lors de sa génération. Ce processus est plus lourd qu'un simple fichier de configuration, c'est certain. Par contre, il permet d'utiliser les mêmes procédures de tests automatisés pour valider les changements d'environnement.

Traiter l'environnement comme du code permet également de profiter des outils de gestions des changements disponibles pour le code (versionnage avec Git, suivi des modifications, code review, etc ...). Ce processus permet aussi d'automatiser un certain nombre de tests de validation de la configuration d'environnement. Concrètement, les données d'environnement sont des données liées. Une fréquence est secourues (POSU) sur un certain nombre de positions. Quand je définis les positions sur lesquelles une fréquence est secourue, le système valide automatiquement la cohérence des données (faire référence à une position qui n'existe pas génère une erreur), et ce, bien en amont du déploiement. Etant donné qu'on veut déployer rapidement et sans douleur, il faut attraper ces erreurs le plus tôt possible dans le processus d'introduction du changement, et si possible de manière automatisée.

### Quelles sont les adaptations locales à réaliser pour mettre en œuvre les profils ETFMS ?

Pour rappel, 4ME, à l'heure actuelle (Janvier 2017) présente 3 services opérationnels. Le premier, MAPPING ou CONTROL ROOM, est responsable d'associer les clients 4ME avec leur type (CWP, Chef de salle, FMP) et leurs secteurs associés. XMAN gère XMAN, et l'ETFMS PROFILE se concentre sur la récupération et l'affichage de ces fameux profils.

Un point important, c'est que le sous module ETFMS PROFILE ne se reconfigure pas en fonction du type de client ou des secteurs associés, contrairement à XMAN. XMAN affiche une liste de vols, filtrée fonction du secteur associé à une position.

Un autre point important, l'applicatif 4ME n'est pas prévu pour fonctionner sans service MAPPING ([voir ce ticket](https://gitlab.com/devteamreims/4ME/issues/154)). Par contre, étant donné que le seul service visible sera ETFMS PROFILE, le service MAPPING, bien que nécessaire, n'a pas besoin d'être tenu à jour.

Le service ETFMS PROFILE a besoin de 2 paramétrages locaux :
* Le premier est l'utilisation d'un certificat B2B NM propre à votre environnement. Cet élément est injecté lors du lancement de l'application conteneurisée.
* Le second est en rapport avec la fonctionnalité d'autocomplétion lors de la recherche par indicatif. Le service de données interroge le B2B NM a intervalles régulier pour récupérer une liste de vols dans un *Airspace*. A ce jour, l'airspace d'interrogation est hardcodé (avec la valeur LFEERMS). Il est prévu d'externaliser ce paramétrage ([voir ticket](https://gitlab.com/devteamreims/4ME/issues/182)). L'introduction de ce paramétrage implique un changement de code plutôt trivial et sera implémenté rapidement.

Faisant suite à un besoin similaire au CRNA-O, un exemple d'implémentation minimal (ETFMS PROFILE uniquement) a été implémenté ici : https://gitlab.asap.dsna.fr/benjamin.beret/4me-minimal-crna-o

### Quelles sont les ressources en ligne que nous devons consulter, ou auxquelles nous devons nous abonner pour prendre en main l'outil ? Pour développer ?

4ME utilise un stack technologique plutôt large, et introduit de nouveaux concepts à chaque étage. Un "tutorial" a été écrit et est [disponible ici](https://gitlab.com/devteamreims/4me.tutorial).

Ce dépôt contient également un répertoire [ressources](https://gitlab.com/devteamreims/4me.tutorial/tree/master/resources) qui contient une liste de vidéos / blogs / forums qui ont alimenté les choix faits jusqu'à présent.

Ce dépôt contient également une [collection d'eBooks](https://gitlab.com/devteamreims/4me.tutorial/tree/master/resources/books).

Un point d'entrée important, qui éclairera grandement un certain nombre de choix, pourrait être la lecture de [The Phoenix Project](https://gitlab.com/devteamreims/4me.tutorial/blob/master/resources/books/The%20Phoenix%20Project_%20A%20Novel%20About%20IT,%20DevOps,%20and%20Helping%20Your%20Business%20Win%20-%20Gene%20Kim%20&%20Kevin%20Behr%20&%20George%20Spafford.mobi). Ce n'est pas un livre technique, mais une nouvelle, dans laquelle les protagonistes sont confrontés aux problématiques récurrentes de l'industrie du logiciel et des systèmes. Vous vous identifierez forcément à un moment ou un autre à une situation déjà rencontrée dans votre travail. Vivement recommandé !

### Quelles sont les spécifications techniques hard/soft pour les postes clients et les serveurs ?

Pour les postes clients, le seul prérequis software est Chromium dans un version plutôt récente. Chromium dispose d'un mode "kioske", en plein écran, où les interactions classiques sont désactivées (click droit par exemple). Niveau hardware, 4GB de RAM et un CPU pas trop asthmatique (type core i3) suffiront. Au CRNA-E, nous sommes partis sur du Linux, qui permet d'avoir un contrôle important sur le processus de lancement du navigateur au démarrage du PC.

Côté serveur, il faut un linux récent, avec Docker version 1.12 minimum. Docker est maintenant disponible sur la majorité des distributions classiques (Ubuntu, debian, RHEL, CentOS). Côté hardware, pareillement, un CPU récent multicore, et 4GB de RAM sont le minimum.

L'écosystème 4ME intègre un système optionnel de collecte et de centralisation des logs (stack [ElasticSearch](https://www.elastic.co/fr/products/elasticsearch) / [FluentD](http://www.fluentd.org/) / [Kibana](https://www.elastic.co/fr/products/kibana)). ElasticSearch est assez consommateur, en RAM notamment. Dans ce cas, 8GB semblent nécessaires pour être confortable.

### Quels sont les pré-requis réseaux et plus particulièrement l'accès internet ?

Plusieurs éléments dépendent d'un accès à internet. Tout d'abord au niveau applicatif, ETFMS PROFILE doit pouvoir joindre le B2B NM. L'applicatif est prévu et testé pour joindre le B2B via un **accès direct sans proxy**.

Ensuite, faute d'infrastructure interne, les livrables sont entreposés sur un dépôt distant accessible via internet (hébergé sur gitlab.com à l'heure actuelle). Il est possible d'amener ces livrables au serveur via un support physique (commandes `docker save` et `docker load` notamment). Cette procédure, bien que possible, est un peu pénible et nécessite un support physique de bonne taille (chaque livrable pesant plusieurs centaines de MB).

### Comment comptez-vous gérer les développements réalisés sur plusieurs sites et assurer la cohérence du produit ?

C'est un point qui reste en débat. Actuellement, 4ME est un projet local CRNA-E. Il n'y a pas de structure projet nationale pour piloter son développement. Il est par contre clair que cette organisation n'est pas pérenne. La DO a la volonté de remonter le projet à un échelon national, mais rien n'a encore été fait.

Sur la gouvernance et la cohérence produit, fonction de ce qu'en pense la DO, il ne semble pas incompatible d'avoir une équipe "distribuée" sur plusieurs sites. Le modèle de l'OpenSource pourrait être une bonne source d'inspiration. Si les divergences locales deviennent trop importantes, il sera toujours possible de "forker" le projet, avec des versions locales suppléant une version "générique" nationale. Bien que plus difficile à gérer, c'est le modèle qu'adoptent les distributions Linux pour intégrer des paquets extérieurs. Ubuntu maintient ses versions, avec ses modifications, intègre les changements du paquet générique dans leur version locale et fait parfois remonter des modifications vers le dépôt "upstream".

### La convergence avec Salto est-elle prévue au niveau logiciel ?

D'un point de vue réseau, une convergence est prévue : le domaine ATM2, qui regroupe ces systèmes (SWIM, ATFCM, Performance) non essentiels à la sécurité des vols. L'architecture technique est en cours de définition côté DTI et devrait fournir des passerelles vers les autres domaines (connexion vers VPN PENS, flux ASTERIX, connexion vers internet via SNARE).

D'un point de vue système (OS, processus de livraison, monitoring, collecte de logs, mise en production), rien n'est encore prévu côté DTI, même si côté CRNA-E nous avons la conviction qu'il faudra tôt ou tard faire converger ces éléments.

D'un point de vue applicatif, il est prévu à terme d'avoir des interactions entre SALTO et 4ME. Le cas des STAM en est le bon exemple. En cible, les STAM seront préparés par le FMPiste sur SALTO. Ces STAM devront être poussés vers les positions de contrôle. SALTO ne disposant pas d'IHM sur les positions de contrôle, 4ME est le candidat naturel pour afficher ces informations. Des travaux sont menés côté SALTO, notamment dans le cadre de SESAR2020. SALTO n'est pas encore en salle de contrôle à Reims et n'implémente pas encore ces fonctionnalités. Le CRNA-E a donc fait le choix de créer un produit minimal répondant au besoin que l'on souhaite mettre en service avant l'été. Ce produit contiendra un module d'IHM pour la visualisation des STAM sur les positions de contrôle, un service de données backend, et un module d'IHM pour le renseignement de ces STAM par la FMP. Lors de la mise en oeuvre des incréments SALTO suivants, le module d'IHM FMP sera remplacé par un bloc d'IHM dans SALTO. L'IHM côté positions de contrôle sera inchangée voire adaptée.
